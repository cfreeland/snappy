/******************************************************************************/
/*These are the variables required to build.***********************************/
/******************************************************************************/

var gulp =                  require('gulp'),
    connect =               require('gulp-connect'),
    gutil =                 require('gulp-util'),
    jshint =                require('gulp-jshint'),
    protractor =            require('gulp-protractor').protractor,
    webdriver_update =      require("gulp-protractor").webdriver_update,
    webdriver_standalone =  require("gulp-protractor").webdriver_standalone,
    server =                require('karma').Server,
    browser_sync =          require('browser-sync').create(),
    watchify =              require('watchify'),
    browserify =            require('browserify'),
    sass =                  require('gulp-sass'),
    source =                require('vinyl-source-stream'),
    path =                  require('path'),
    del =                   require('del'),
    _ =                     require('lodash'),
    stylish =               require('jshint-stylish'),
    globby =                require('globby'),
    run =                   require('run-sequence');

/******************************************************************************/
/*Configuration variables.*****************************************************/
/******************************************************************************/

var hostPort = 8888;

/******************************************************************************/
/*These are the variables to decide what to bundle.****************************/
/******************************************************************************/

var sassEntry = './app/styles/style.scss';
var sassDestn = './dist/app/styles';
var sassFiles = 'app/styles/*.scss';

var appCode = ['app/**/*.js', '!app/bower_components/**'];
var uiAssets = ['app/**/*.html', 'app/img/*', 'app/*.ico', '!app/**/README.md', '!app/bower_components'];
var bowerAssets = ['app/bower_components/**'];

/******************************************************************************/
/*Tasks************************************************************************/
/******************************************************************************/

gulp.task('default', function() {
    run('clean-dist', 'webupdate', 'watch');
});

gulp.task('watch', ['local', 'watchify-karma-tests', 'watchify-e2e-tests', 'watchify-site', 'ui-assets', 'sass', 'bower-assets'], function() {
    run('browsers');
    gulp.watch(uiAssets, ['ui-assets']).on('change', browser_sync.reload);
    gulp.watch(sassFiles, ['sass']).on('change', browser_sync.reload);
});

gulp.task('webupdate', webdriver_update);

gulp.task('webstart',  webdriver_standalone);

gulp.task('browsers', function() {
    browser_sync.init({
        proxy: 'localhost:' + hostPort
    });
});

gulp.task('local', function() {
    connect.server({
        root: 'dist/app/',
        port: hostPort
    });
});

gulp.task('lint', function() {
    return gulp.src(appCode, {base: '.'})
        .pipe(jshint())
        .pipe(jshint.reporter(stylish));
});

gulp.task('test', function(done) {
    new server({
        configFile: __dirname + '/karma.conf.js',
        singleRun: true
    }, done).start();
});

gulp.task('e2e', function() {
    gulp.src(["dist/app/bundle_e2e.js"])
       .pipe(protractor({
           configFile: "conf.js",
           args: ['--baseUrl', 'http://127.0.0.1:' + hostPort]
       }))
       .on('error', function(e) { throw e; });
});

gulp.task('browserify', function() {
    return browserify('./app/app.js')
            .transform('browserify-ngannotate')
            .bundle()
            .pipe(source('bundle.js'))
            .pipe(gulp.dest('./dist/app'));
});

gulp.task('watchify-site', function() {
    return watchifyCreator('./app/app.js', 'bundle.js', ['lint', 'e2e', 'test']);
});

gulp.task('watchify-karma-tests', function() {
    var bundleName = 'bundle_karma.js';
    var testFiles = globby.sync(['./app/**/*.test.js', '!./dist/app/' + bundleName]);
    return watchifyCreator(testFiles, bundleName, 'test');
});

gulp.task('watchify-e2e-tests', function() {
    var bundleName = 'bundle_e2e.js';
    var testFiles = globby.sync(['./app/tests/*.js', '!./dist/app/' + bundleName]);
    return watchifyCreator(testFiles, bundleName, 'e2e');
});

gulp.task('sass', function() {
    return gulp.src(sassEntry)
                .pipe(sass())
                .pipe(gulp.dest(sassDestn));
});

gulp.task('ui-assets', function() {
    return gulp.src(uiAssets, {base: '.'})
                .pipe(gulp.dest('dist'));
});

gulp.task('bower-assets', function() {
    return gulp.src(bowerAssets, {base: '.'})
            .pipe(gulp.dest('dist'));
});

gulp.task('clean-dist', function() {
    del('./dist/*');
});

function watchifyCreator(files, bundleName, tests) {
    var watchified = watchify(browserify(files, _.extend({debug: true}, watchify.args)));
    watchified
            .transform('browserify-ngannotate')
            .transform('require-globify');

    function update() {
        return watchified
                .bundle()
                .on('error', gutil.log.bind(gutil, 'Browserify error'))
                .pipe(source(bundleName))
                .pipe(gulp.dest('./dist/app'));
    }

    watchified.on('update', function() {
        update();
        if (tests.length > 0) {
            run(tests);
        }
    });
    watchified.on('log', gutil.log);

    return update();
}
