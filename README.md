# README #

### Standard framework for testing and deployment of an angular site. ###

* Quick summary
* v0.9

### How do I get set up? ###

* Ensure that you have nodejs, npm, bower, karma-cli, and karma installed globally

    -Windows Users-

        ~navigate to https://nodejs.org/download/
        ~run the download file to  put node and npm on your system
        ~enter command line and perform the following commands

            npm install bower -g
            npm install karma-cli -g
            npm install karma -g

    -Linux Users-
        ~install nodejs using your distro's package manager (e.g. apt-get)
        ~perform the following commands:

            npm install bower -g
            npm install karma-cli -g
            npm install karma -g

* Clone the repository
* Enter the repository directory and run the following commands
* npm install
* bower install